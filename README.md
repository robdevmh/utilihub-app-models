# Git Submodules
sample application with submodule

# How to initialized an application with submodule
to initialize a repository, you can do it by cloning the application first, once you have the application in your local machine, you can run the git submodule update, eg:
```sh
git clone git@app-repo.git application
cd application
git submodule update --init --recursive
```

this will automatically pull all the gitsubmodule from the remote branch based on the hash commit from your main repository

# How to add submodule
to add a submodule, use the command below:

```sh
git submodule add git@repo.git directoryName
```

once the command was added, it will automatically pull he main branch (probably master branch), to the current directory

also it will create a file called `.gitmodules`, which represents all the submodule for this repository.

```sh
[submodule "directoryName"]
	path = directoryName
	url = git@repo.git
```

## Submodule

### Update a submodule inside app
to update a submodule inside the application, you can go to the submodule directory which in our case called `directoryName`

```sh
cd directoryName
```

once you are in that directory, you can now use the usual `git` commands such as:

- git add
- git commit
- git push
- git reset
- git pull

and etc, and once you made changes to the submodule repository and commit your changes and all., it *WONT* update your **Application Repository**

so to update your application go back to your application directory / repository

```sh
# assuming you are still in the submodule directory and 
# one level above the main/application reposiroy

cd ../
```

see [How to update the application to update a submodule](#markdown-header-how-to-update-the-application-to-update-a-submodule)

### Updating a submodule outside the application
to update a submodule outside the application, you can `clone` the repository of your submodule and treat it as usual application, you can do any git commands such as: 

- git add
- git commit
- git push
- git reset
- git pull

and etc.. **BUT** it won't update the application that uses this repo as a submodule but instead you have to go to that directory and update it by yourself (unless you want to create script for this but out of scope).

## How to update the application to update a submodule
to grab the latest update to the existing submodule and update your application, there are a couple of ways to do this

### 1. Manually pull
- go to the submodule directory inside your app, and do a `git pull`.
- once the submodule updated from the remote branch
- go back to your root directory
- once you are in the root directory of your application
- you can try and do `git status` and you'll see that the `git` will tell you that there are `changes/modified` in the **submodule folder**
- now all you need to do is commit this changes and push/update your remote branch

### 2. For a busy person and dont actually mind the changes happen
- go to root directory
- A. run `git submodule foreach git pull origin master`, assuming it's the master branch and all
- B. run `git submodule update --remote --merge` *(same thing as A. but from newer version of git)*
- once you run the command above, all you need to do is commit this changes and push/update your remote branch sample: 
```sh
    # directoryName - the folder of submodule created at the start

    git add directoryName
    git commit -m "update submodule directoryName"
    git push origin master
```
